---
title: Adhésions
---
# Gestion des adhésions

---

!!! Ces informations sont pour nos adhérentes et adhérents. L'adhésion se fait suite à une session d'essai.

## 1. Envoi des documents sur myffvolley
Vous pouvez créer votre compte sur l'application myffvolley [my.ffvolley.org](https://my.ffvolley.org).

## 2. Paiement sur helloasso

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/leo-nantes-volley-ball/adhesions/inscriptions-volley/widget-vignette" style="text-align: center;width: 100%; height: 500px; border: none;"></iframe>