---
title: Équipe 3
body_classes: 'title-center title-h1h2'
slug: 'equipe3'
---

## Leo Nantes Volley ball - Équipe 3
En 2023 Léo Lagrange relance un recrutement pour une nouvelle équipe.
Dans cette équipe pas de niveaux requis, venez découvrir les subtilités du volley !

!!! Entrainements le vendredi de 20h à 22h !


