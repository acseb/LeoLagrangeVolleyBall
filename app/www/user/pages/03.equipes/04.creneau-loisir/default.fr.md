---
title: 'Créneau Loisir'
body_classes: 'title-center title-h1h2'
slug: creneau-loisir
---

## Leo Nantes Volley ball - Créneau Loisir
Pour pratiquer sans engagements et en toute décontraction.  
Possible de s'inscrire pour venir faire sa session de sport hebdomadaire ou pour peaufiner votre entrainement

!!! Créneau ouvert le vendredi de 18h30 à 20h !


