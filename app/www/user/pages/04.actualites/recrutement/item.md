---
title: Recrutements
publish_date: 21-07-2024 13:00
date: 21-07-2024 13:00
---


Leo Nantes Volley Ball ça continue pour la saison 2024-2025 !

Nous vous proposons un nouveau créneau de pratique en toute liberté. Venez pratiquez quand et comme vous voulez, pour seulement 70€ l'année.
En certificat médical sera tout de même nécessaire. 

===


🗓️ Le créneau est le vendredi soir de 18h30 à 20h au Complexe Sportif Gaston Turpin

N'hésitez pas à nous contacter [leovolley@mailo.com](mailto:leovolley@mailo.com) pour plus de détails 

