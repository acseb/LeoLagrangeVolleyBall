---
title: Actualités
hero_image: filet-volley.jpg
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: false
    url_taxonomy_filters: true
admin:
    children_display_order: collection
hero_classes: 'hero-tiny text-light parallax overlay-dark-gradient'
---

## Les actualités du club Léo Nantes Volley Ball