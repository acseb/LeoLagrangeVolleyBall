---
title: Présentation
class: bg-gray
image_align: center
---

Léo Nantes Volley Ball est un club de volley amateur situé dans la quartier de Malakoff Saint Donation.
Nous accueillons de nombreux joueurs et joueuses pour pratiquer et s'améliorer au volley dans la bonne humeur !

Nous présentons 3 équipes en compétition [CompetLib du Comite 44](https://www.comite44volleyball.org/competlib) et avons un créneau de pratique sans encadrement.

Il est encore possible d'adhérer pour le [Créneau Loisir](../../equipes/creneau-loisir) !
!!! [fa=fa-calendar-check-o /]  le **vendredi** de **18h30h** à **20h** ! 