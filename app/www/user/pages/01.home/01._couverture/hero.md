---
title: Couverture
body_classes: modular
media_order: couv_leo.jpg
hero_classes: 'hero-sm text-light parallax overlay-dark-gradient'
hero_image: couv_leo.jpg
---

# Leo Nantes Volley Ball


## 50 ans de pratique du volley-ball à Nantes !